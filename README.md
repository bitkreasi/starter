# NextJS

To start contributing on the NextJS side, you need to go to the NextJS project folder.

Go to NextJS project:

```
cd nextjs
```

Make sure your terminal is in NextJS project. Example: `/starter/nextjs`

# Fastify

To start contributing on the Fastify side, you need to go to the Fastify project folder.

Go to Fastify project:

```
cd fastify
```

Make sure your terminal i in Fastify project. Example: `/starter/fastify`
